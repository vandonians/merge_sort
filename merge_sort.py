def merge_sort(list_):
    if len(list_) < 2:
        return list_

    middle = len(list_) // 2
    left = merge_sort(list_[:middle])
    right = merge_sort(list_[middle:])

    merged_list = reduce(left, right)
    return merged_list


def reduce(left, right):
    #if len(left) == 0 or len(right) == 0:
    #    return []

    result = []
    left_index = 0
    right_index = 0
    #final_length = len(left) + len(right)

    while True: #(len(result) < final_length):
        if left[left_index] < right[right_index]:
            result.append(left[left_index])
            left_index += 1
        else:
            result.append(right[right_index])
            right_index += 1

        if left_index == len(left):
            result.extend(right[right_index:])
            break
        if right_index == len(right):
            result.extend(left[left_index:])
            break
    return result