import random
import time
import matplotlib.pyplot as plt
import merge_sort

lengths_to_run = [1001, 2000, 4000, 8000, 16000, 32000, 64000, 128000, 256000, 512000, 1000000]
results = []

for length in lengths_to_run:
    list_ = list(range(length, 0, -1))
    random.shuffle(list_)
    start = time.time()
    merge_sort.merge_sort(list_)
    end = time.time()
    results.append(end - start)

print(results)

plt.plot(lengths_to_run, results,  label='average')
plt.legend()
plt.show()